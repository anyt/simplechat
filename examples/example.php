<?php

require_once __DIR__.'./../vendor/autoload.php';

use SimpleChat\SimpleChat;
use SimpleChat\DataAudit\DataAuditOutputLogger;

// simple facade to work with chat
$chatManager = new SimpleChat(25, new DataAuditOutputLogger());

// create user
$organizer = $chatManager->createUser('Andrey');

// create chat room
$chatRoom = $chatManager->createRoom();

// organizer joins chat room
$chatRoom
    ->join($organizer);

// organizer share's link with room Id with other users
$roomId = $chatRoom->getId();

// create two more users
$userOne = $chatManager->createUser('Petya');
$userTwo = $chatManager->createUser('Vasya');

// user one and two joined chat by chat id from link
$chatManager->findOrCreateRoom($roomId)
    ->join($userOne)
    ->join($userTwo);

// user one enabled audio and video
$userOne->getChatSettings()
    ->setAudioEnabled(true)
    ->setVideoEnabled(true);

$chatRoom
    ->sendMessage('Hello!', $userOne)
    ->sendMessage('Hello Guys!', $userTwo)
    ->sendMessage('Hello! Lets begin our meeting.', $userOne)
    ->sendMessage('Ok. Lets start. I\'l share my screen', $organizer);

// organizer started screen share
$organizer->getChatSettings()
    ->setScreenShareEnabled(true);

$chatRoom
    ->sendMessage('I see.', $userOne);

echo "\n-----------\n\n";
echo $chatRoom->getTextChatListing();
echo "\n-----------\n\n";

// all users leave chat
$chatRoom
    ->leave($userTwo)
    ->leave($userOne)
    ->leave($organizer);

echo "\n-----------\n";
echo "\nchat is empty after everyone out\n\n";
echo $chatRoom->getTextChatListing();
