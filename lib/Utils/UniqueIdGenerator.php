<?php

namespace SimpleChat\Utils;

class UniqueIdGenerator
{
    /**
     * @param string $prefix
     * @return string
     */
    public static function generateId($prefix = '')
    {
        return uniqid($prefix);
    }
}
