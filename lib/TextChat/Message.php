<?php

namespace SimpleChat\TextChat;

use SimpleChat\ChatUser;

class Message
{
    /** @var string */
    private $message;

    /** @var ChatUser */
    private $from;

    /** @var \DateTime */
    private $createdAt;

    /**
     * @param $message
     * @param ChatUser $from
     */
    public function __construct($message, ChatUser $from)
    {
        $this->message = $message;
        $this->from = $from;
        $this->createdAt = new \DateTime();

    }

    public function __toString()
    {
        return sprintf(
            '%s | %s say: "%s"',
            $this->createdAt->format('Y-m-d H:i:s'),
            $this->from->getName(),
            $this->message
        );
    }
}
