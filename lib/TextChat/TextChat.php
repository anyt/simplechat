<?php

namespace SimpleChat\TextChat;

class TextChat
{
    private $messages = [];

    /**
     * @param Message $message
     */
    public function send(Message $message)
    {
        $this->messages[] = $message;
    }

    public function getListing()
    {
        $listing = '';
        foreach ($this->messages as $message) {
            $listing .= $message."\n";
        }

        return $listing;
    }
}
