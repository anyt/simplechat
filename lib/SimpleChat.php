<?php

namespace SimpleChat;

use SimpleChat\DataAudit\AuditableChatRoom;
use SimpleChat\DataAudit\DataAuditOutputLogger;

class SimpleChat
{
    /** @var  ChatRoomInterface[] */
    private $rooms;

    /** @var integer */
    private $userLimit;

    /** @var DataAuditOutputLogger */
    private $logger;

    /**
     * @param int $userLimit
     * @param DataAuditOutputLogger $logger
     */
    public function __construct($userLimit = 25, DataAuditOutputLogger $logger)
    {
        $this->userLimit = $userLimit;
        $this->logger = $logger;
    }

    /**
     * @return ChatRoomInterface
     */
    public function createRoom() : ChatRoomInterface
    {
        $room = new ChatRoom($this->userLimit);

        $room = new AuditableChatRoom($room, $this->logger); // enable audit on chat room

        $this->rooms[$room->getId()] = $room;

        return $room;
    }

    /**
     * @param string $name
     * @return ChatUser
     */
    public function createUser(string $name)
    {
        return new ChatUser($name);
    }

    /**
     * If no active rooms found by id - just create new empty room
     *
     * @param string $roomId
     * @return ChatRoomInterface
     */
    public function findOrCreateRoom(string $roomId)
    {
        return array_key_exists($roomId, $this->rooms) ? $this->rooms[$roomId] : $this->createRoom();
    }
}
