<?php

namespace SimpleChat;

interface ChatRoomInterface
{
    /**
     * @param ChatUser $user
     * @return $this
     */
    public function join(ChatUser $user);

    /**
     * @param ChatUser $user
     * @return $this
     */
    public function leave(ChatUser $user);

    /**
     * @return ChatUser
     */
    public function getOrganizer();

    /**
     * @return ChatUser[]
     */
    public function getUsers();

    /**
     * @return string
     */
    public function getId();

    /**
     * @param $message
     * @param ChatUser $user
     * @return $this
     */
    public function sendMessage($message, ChatUser $user);

    /**
     * @return string
     */
    public function getTextChatListing();
}
