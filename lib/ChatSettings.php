<?php

namespace SimpleChat;

class ChatSettings
{
    /** @var  bool */
    private $videoEnabled;

    /** @var  bool */
    private $audioEnabled;

    /** @var  bool */
    private $screenShareEnabled;

    public function __construct(
        bool $videoEnabled = false,
        bool $audioEnabled = false,
        bool $screenShareEnabled = false
    ) {
        $this->videoEnabled = $videoEnabled;
        $this->audioEnabled = $audioEnabled;
        $this->screenShareEnabled = $screenShareEnabled;
    }

    /**
     * @return boolean
     */
    public function isScreenShareEnabled()
    {
        return $this->screenShareEnabled;
    }

    /**
     * @param boolean $screenShareEnabled
     * @return $this
     */
    public function setScreenShareEnabled($screenShareEnabled)
    {
        $this->screenShareEnabled = $screenShareEnabled;
        
        return $this;
    }

    /**
     * @return boolean
     */
    public function isAudioEnabled()
    {
        return $this->audioEnabled;
    }

    /**
     * @param boolean $audioEnabled
     * @return $this
     */
    public function setAudioEnabled($audioEnabled)
    {
        $this->audioEnabled = $audioEnabled;
        
        return $this;
    }

    /**
     * @return boolean
     */
    public function isVideoEnabled()
    {
        return $this->videoEnabled;
    }

    /**
     * @param boolean $videoEnabled
     * @return $this
     */
    public function setVideoEnabled($videoEnabled)
    {
        $this->videoEnabled = $videoEnabled;


        return $this;
    }
}
