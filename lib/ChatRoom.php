<?php

namespace SimpleChat;

use SimpleChat\TextChat\Message;
use SimpleChat\TextChat\TextChat;
use SimpleChat\Utils\UniqueIdGenerator;

class ChatRoom implements ChatRoomInterface
{
    /** @var ChatUser[] */
    private $users;

    /** @var ChatUser */
    private $organizer;

    /** @var string */
    private $id;

    /** @var integer */
    private $userLimit;

    /** @var TextChat */
    private $textChat;

    /**
     * @param $userLimit
     */
    public function __construct($userLimit)
    {
        $this->userLimit = $userLimit;
        $this->id = UniqueIdGenerator::generateId('chat_room_');
        $this->textChat = new TextChat;
    }

    /**
     * @param ChatUser $user
     * @return $this
     */
    public function join(ChatUser $user)
    {
        if (empty($this->users)) {
            $this->organizer = $user;
        }
        $this->validateUser($user);
        $this->addUser($user);

        return $this;
    }

    /**
     * @param ChatUser $user
     * @return $this
     */
    public function leave(ChatUser $user)
    {
        unset($this->users[$user->getId()]);

        if (0 === count($this->users)) {
            $this->textChat = new TextChat();
        }

        return $this;
    }

    /**
     * @return ChatUser
     */
    public function getOrganizer()
    {
        return $this->organizer;
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param ChatUser $user
     * @return $this
     */
    protected function addUser(ChatUser $user)
    {
        $this->users[$user->getId()] = $user;

        return $this;
    }

    /**
     * @param ChatUser $user
     * @throws \Exception
     */
    protected function validateUser(ChatUser $user)
    {
        if (count($this->users) >= $this->userLimit) {
            throw new \Exception(
                sprintf('Room is full, only %s users allowed in a single chat room', $this->userLimit)
            );
        }
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $message
     * @param ChatUser $user
     * @return $this
     */
    public function sendMessage($message, ChatUser $user)
    {
        $this->textChat->send(new Message($message, $user));

        return $this;
    }

    public function getTextChatListing()
    {
        return $this->textChat->getListing();
    }
}
