<?php

namespace SimpleChat\DataAudit;

use SimpleChat\ChatRoomInterface;
use SimpleChat\ChatUser;

class AuditableChatRoom implements ChatRoomInterface
{
    /**
     * @var ChatRoomInterface
     */
    private $chatRoom;

    /**
     * @var DataAuditLoggerInterface
     */
    private $logger;

    /**
     * @param ChatRoomInterface $chatRoom
     * @param DataAuditLoggerInterface $logger
     */
    public function __construct(ChatRoomInterface $chatRoom, DataAuditLoggerInterface $logger)
    {
        $this->chatRoom = $chatRoom;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function join(ChatUser $user)
    {
        $this->chatRoom->join($user);

        $this->logger->log(sprintf('User "%s" joined "%s" chat', $user->getId(), $this->chatRoom->getId()));

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function leave(ChatUser $user)
    {
        $this->chatRoom->leave($user);

        $this->logger->log(sprintf('User "%s" leaved "%s" chat', $user->getId(), $this->chatRoom->getId()));

        if (0 === count($this->chatRoom->getUsers())) {
            $this->logger->log(sprintf('Chat "%s" ended.', $this->chatRoom->getId()));
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getOrganizer()
    {
        return $this->chatRoom->getOrganizer();
    }

    /**
     * @inheritdoc
     */
    public function getUsers()
    {
        return $this->chatRoom->getUsers();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->chatRoom->getId();
    }

    /**
     * @inheritdoc
     */
    public function sendMessage($message, ChatUser $user)
    {
        $this->chatRoom->sendMessage($message, $user);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getTextChatListing()
    {
        return $this->chatRoom->getTextChatListing();
    }
}
