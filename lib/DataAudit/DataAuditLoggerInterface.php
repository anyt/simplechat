<?php

namespace SimpleChat\DataAudit;

interface DataAuditLoggerInterface
{
    /**
     * @param string $message
     */
    public function log($message);
}
