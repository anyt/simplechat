<?php

namespace SimpleChat\DataAudit;

class DataAuditOutputLogger implements DataAuditLoggerInterface
{
    /**
     * @inheritdoc
     */
    public function log($message)
    {

        $dateTime = (new \DateTime())->format('Y-m-d H:i:s');

        echo $dateTime.' | '.$message."\n";
    }
}
