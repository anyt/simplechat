<?php

namespace SimpleChat;

use SimpleChat\Utils\UniqueIdGenerator;

class ChatUser
{
    /** @var string */
    private $id;

    /** @var  string */
    private $name;

    /** @var ChatSettings */
    private $chatSettings;

    public function __construct(string $name)
    {
        $this->id = UniqueIdGenerator::generateId('user_');
        $this->name = $name;
        $this->chatSettings = new ChatSettings();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ChatSettings
     */
    public function getChatSettings()
    {
        return $this->chatSettings;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}
